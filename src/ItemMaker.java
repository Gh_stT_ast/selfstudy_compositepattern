
public class ItemMaker {

	public static void main(String[] args) {
		
		Item dumbbell = new Item("dumbbell", 100);
		Item microwave = new Item("microwave", 10);
		CrateComponent littleCrate1 = new Crate("LITTLE_1");
		CrateComponent littleCrate2 = new Crate("LITTLE_2");
		
		for (int i = 0; i < 12; i++) {
			littleCrate1.add(new Item("toaster", 2));
		}
		for (int i = 0; i < 4; i++) {
			littleCrate2.add(microwave);
			littleCrate2.add(new Item("blender", 5));
		}
		CrateComponent bigCrate = new Crate("BIG");
		
		CrateComponent reallyBigCrate = new Crate("REALLY BIG");
		
		bigCrate.add(littleCrate1);
		bigCrate.add(littleCrate2);
		reallyBigCrate.add(bigCrate);
		
		
		InventoryChecker checker = new InventoryChecker(reallyBigCrate);
		checker.getItemList();
		reallyBigCrate.add(dumbbell);
		littleCrate2.remove(microwave);
		checker.getItemList();
	}
}
