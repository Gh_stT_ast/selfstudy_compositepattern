
public class Item extends CrateComponent{
	String name;
	int weight;
	int count;

	public Item(String name, int weight) {
		this.name = name;
		this.weight = weight;
		this.count = 1;
	}
	
	public String getName() { return name; }

	public int getWeight() { return weight; }

	public int getCount() { return count; }

	public void displayItemInfo() {
		System.out.println(getName() + " | " + getWeight() + 
				" lbs");
	}
}
