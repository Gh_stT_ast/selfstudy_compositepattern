import java.util.ArrayList;
import java.util.Iterator;

public class Crate extends CrateComponent{
	
	private ArrayList<CrateComponent> items = new ArrayList<CrateComponent>();
	
	String name;
        int weight; // default crate weight
	int count;
	
	public Crate(String name) {
		this.name = name;
		this.weight = 10;
		this.count = items.size(); // items is initialized, so will return 0. Did it this way to keep logic consistent, i.e we're calculating count by the size of items
	}
	
	public String getName() { return this.name; }
	public int getWeight() { 
            int total_weight  = this.weight;
            Iterator iterator = items.iterator();
            CrateComponent c;
            while(iterator.hasNext()) {
                c = (CrateComponent)iterator.next();
	        total_weight += c.getWeight();
	    }
            
            return total_weight; // if crate doesn't contain items, it'll return it's default weight of 10
        }

	public int getCount() { return items.size(); }

	public void add(CrateComponent item) {
		items.add(item);
	}
	
	public void remove(CrateComponent item) {
		items.remove(item);
	}
	
	public CrateComponent getItem (int index) { return (CrateComponent)items.get(index); }
	
	public void displayItemInfo() {
		System.out.println("\n" + getName() + " CRATE");
		System.out.println("=================");
		System.out.println("Weight: " + getWeight() + " lbs / " +
				   "Count: " + getCount() + " items");
		
		System.out.println("------ITEMS------");
		Iterator iterator = items.iterator();
		CrateComponent c;
		while(iterator.hasNext()) {
			c = (CrateComponent)iterator.next();
			c.displayItemInfo();
		}
	}
}
