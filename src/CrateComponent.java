
public abstract class CrateComponent {
	
	public String getName() {
		throw new UnsupportedOperationException();
	}
	public int getWeight() {
		throw new UnsupportedOperationException();
	}
	
	public int getCount() {
		throw new UnsupportedOperationException();
	}
	
	public void add(CrateComponent item) {
		throw new UnsupportedOperationException();
	}
	
	public void remove(CrateComponent item) {
		throw new UnsupportedOperationException();
	}
	
	public void displayItemInfo() {
		throw new UnsupportedOperationException();
	}
}
